const assert = require('assert')
const rlp = require('rlp')
const crypto = require('crypto')
const createKeccakHash = require('keccak')
const isHexPrefixed = require('is-hex-prefixed')
const secp256k1 = require('secp256k1')
const stripHexPrefix = require('strip-hex-prefix')
const BN = require('bn.js')

exports.BN = BN

exports.getPowHeader = function (block) {
  let powHeader = [
    exports.int2Buffer(block.number),
    Buffer.from(block.parentHash, 'hex'),
    Buffer.from(block.extraData)
  ]

  powHeader = Buffer.concat(powHeader)
  return powHeader
}

exports.parseArgv = function (argv) {
  let _argv = {}
  argv.forEach((param) => {
    switch (param) {
      case 'tx':
        _argv['tx'] = 1
        break
      case 'pow':
        _argv['pow'] = 1
        break
      default:
        break
    }
  })

  return _argv
}

exports.zeros = function (num) {
  return '0'.repeat(num)
}

exports.zerosBuf = function (bytes) {
  return Buffer.allocUnsafe(bytes).fill(0).toString('hex')
}

/**
 * Creates SHA-3 (Keccak) hash of the input [OBSOLETE]
 * @param {Buffer|Array|String|Number} a the input data
 * @param {Number} [bits=256] the SHA-3 width
 * @return {Buffer}
 */
exports.sha3 = function (a, bits) {
  a = exports.toBuffer(a)
  if (!bits) bits = 256
  return createKeccakHash('keccak' + bits).update(a).digest()
}

/**
 * Converts an `Number` to a `Buffer`
 * @param {Number} i
 * @return {Buffer}
 */
exports.int2Buffer = function (i) {
  const hex = exports.intToHex(i)
  return Buffer.from(exports.padToEven(hex.slice(2)), 'hex')
}

exports.padToEven = function (value) {
  let a = value; // eslint-disable-line
  if (typeof a !== 'string') {
    throw new Error(`[secjs-util] while padding to even, value must be string, is currently ${typeof a}, while padToEven.`)
  }
  if (a.length % 2) {
    a = `0${a}`
  }
  return a
}

/**
 * Converts a `Number` into a hex `String`
 * @param {Number} i
 * @return {String}
 */
exports.intToHex = function (i) {
  let hex = i.toString(16); // eslint-disable-line
  return `0x${hex}`
}

/**
 * Creates Keccak hash of the input
 * @param {Buffer|Array|String|Number} a the input data
 * @param {Number} [bits=256] the Keccak width
 * @return {Buffer}
 */
exports.keccak = function (a, bits) {
  a = exports.toBuffer(a)
  if (!bits) bits = 256
  return createKeccakHash('keccak' + bits).update(a).digest()
}

/**
 * Creates SHA-3 hash of the RLP encoded version of the input
 * @param {Buffer|Array|String|Number} a the input data
 * @return {Buffer}
 */
exports.rlphash = function (a) {
  return this.keccak(rlp.encode(a))
}

/**
 * Checks if the address is a valid. Accepts checksummed addresses too
 * @param {String} address
 * @return {Boolean}
 */
exports.isValidAddress = function (address) {
  return /^0x[0-9a-fA-F]{40}$/.test(address)
}

exports.genKeySets = function () {
  // let key = ec.genKeyPair()
  // let privKey = key.getPrivate().toString('hex')
  let privKey = crypto.randomBytes(32).toString('hex')
  let privKeyBuffer = Buffer.from(privKey, 'hex')
  let publicKeyBuffer = exports.privateToPublic(privKeyBuffer)
  let addressBuffer = exports.publicToAddress(publicKeyBuffer)

  let publicKey = publicKeyBuffer.toString('hex')
  let address = addressBuffer.toString('hex')
  return {
    privKey: privKey,
    publicKey: publicKey,
    address: address
  }
}

/**
 * Returns the SEC public key of a given private key
 * @param {Buffer} privateKey A private key must be 256 bits wide
 * @return {Buffer}
 */
exports.privateToPublic = function (privateKey) {
  privateKey = exports.toBuffer(privateKey)
  // skip the type flag and use the X, Y points
  return secp256k1.publicKeyCreate(privateKey, false).slice(1)
}

/**
 * Returns the SEC address of a given public key.
 * Accepts "SEC public keys" and SEC1 encoded keys.
 * @param {Buffer} pubKey The two points of an uncompressed key, unless sanitize is enabled
 * @param {Boolean} [sanitize=false] Accept public keys in other formats
 * @return {Buffer}
 */
exports.publicToAddress = function (pubKey, sanitize) {
  pubKey = exports.toBuffer(pubKey)
  if (sanitize && (pubKey.length !== 64)) {
    pubKey = secp256k1.publicKeyConvert(pubKey, false).slice(1)
  }
  assert(pubKey.length === 64)
  // Only take the lower 160bits of the hash
  return exports.keccak(pubKey).slice(-20)
}

/**
 * Converts a `Buffer` to a `Number`
 * @param {Buffer} buf
 * @return {Number}
 * @throws If the input number exceeds 53 bits.
 */
exports.buffer2Int = function (buf) {
  return new BN(exports.toBuffer(buf)).toNumber()
}

/**
 * Is the string a hex string.
 *
 * @method check if string is hex string of specific length
 * @param {String} value
 * @param {Number} length
 * @returns {Boolean} output the string is a hex string
 */
exports.isHexString = function (value, length) {
  if (typeof (value) !== 'string' || !value.match(/^0x[0-9A-Fa-f]*$/)) {
    return false
  }
  if (length && value.length !== 2 + 2 * length) { return false }
  return true
}

/**
 * ECDSA public key recovery from signature
 * @param {Buffer} msgHash
 * @param {Number} v
 * @param {Buffer} r
 * @param {Buffer} s
 * @return {Buffer} publicKey
 */
exports.ecrecover = function (msgHash, v, r, s) {
  const signature = Buffer.concat([exports.setLengthLeft(r, 32), exports.setLengthLeft(s, 32)], 64)
  const recovery = v - 27
  if (recovery !== 0 && recovery !== 1) {
    throw new Error('Invalid signature v value')
  }
  const senderPubKey = secp256k1.recover(msgHash, signature, recovery)
  return secp256k1.publicKeyConvert(senderPubKey, false).slice(1)
}

/**
 * Returns the address of a given public key.
 * Accepts "public keys" and SEC1 encoded keys.
 * @param {Buffer} pubKey The two points of an uncompressed key, unless sanitize is enabled
 * @param {Boolean} [sanitize=false] Accept public keys in other formats
 * @return {Buffer}
 */
exports.public2Address = function (pubKey, sanitize) {
  pubKey = exports.toBuffer(pubKey)
  if (sanitize && (pubKey.length !== 64)) {
    pubKey = secp256k1.publicKeyConvert(pubKey, false).slice(1)
  }
  assert(pubKey.length === 64)
  // Only take the lower 160bits of the hash
  return exports.keccak(pubKey).slice(-20)
}

/**
 * ECDSA sign
 * @param {Buffer} msgHash
 * @param {Buffer} privateKey
 * @return {Object}
 */
exports.ecsign = function (msgHash, privateKey) {
  const sig = secp256k1.sign(msgHash, privateKey)

  const ret = {}
  ret.r = sig.signature.slice(0, 32)
  ret.s = sig.signature.slice(32, 64)
  ret.v = sig.recovery + 27
  return ret
}

// only compares one level
exports.compreJsons = function (obj1, obj2) {
  let flag = true
  function compre (obj1, obj2) {
    if (Object.keys(obj1).length !== Object.keys(obj2).length) {
      flag = false
    } else {
      for (let x in obj1) {
        if (obj2.hasOwnProperty(x)) {
          if (obj1[x] !== obj2[x]) {
            compre(obj1[x], obj2[x])
          }
        } else {
          flag = false
        }
      }
    }
    if (flag === false) {
      return false
    } else {
      return true
    }
  }
  return compre(obj1, obj2)
}

exports.toBuffer = function (v) {
  if (!Buffer.isBuffer(v)) {
    if (Array.isArray(v)) {
      v = Buffer.from(v)
    } else if (typeof v === 'string') {
      if (exports.isHexString(v)) {
        v = Buffer.from(exports.padToEven(stripHexPrefix(v)), 'hex')
      } else {
        v = Buffer.from(v)
      }
    } else if (typeof v === 'number') {
      v = exports.int2Buffer(v)
    } else if (v === null || v === undefined) {
      v = Buffer.allocUnsafe(0)
    } else if (BN.isBN(v)) {
      v = v.toArrayLike(Buffer)
    } else if (v.toArray) {
      // converts a BN to a Buffer
      v = Buffer.from(v.toArray())
    } else {
      throw new Error('invalid type')
    }
  }
  return v
}

/**
 * Left Pads an `Array` or `Buffer` with leading zeros till it has `length` bytes.
 * Or it truncates the beginning if it exceeds.
 * @method lsetLength
 * @param {Buffer|Array} msg the value to pad
 * @param {Number} length the number of bytes the output should be
 * @param {Boolean} [right=false] whether to start padding form the left or right
 * @return {Buffer|Array}
 */
exports.setLengthLeft = function (msg, length, right) {
  const buf = exports.zeros(length)
  msg = exports.toBuffer(msg)
  if (right) {
    if (msg.length < length) {
      msg.copy(buf)
      return buf
    }
    return msg.slice(0, length)
  } else {
    if (msg.length < length) {
      msg.copy(buf, length - msg.length)
      return buf
    }
    return msg.slice(-length)
  }
}

exports.stripHexPrefix = function (str) {
  if (typeof str !== 'string') {
    return str
  }
  return isHexPrefixed(str) ? str.slice(2) : str
}
