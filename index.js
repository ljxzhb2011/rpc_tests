const jayson = require('jayson')
const ip = require('ip')
const utils = require('./utils.js')

const myIp = ip.address()
const client = jayson.client.http({
  host: myIp,  
  port: 3000
})

// const UserInfo = utils.genKeySets()

const UserInfo = {
  privKey: '962969f060fe6ff79dab7a4cea84761f58dce68512c2b9f4785418bf9cf36c85',
  publicKey: '997dca17676d64d56ac01e2846e01ba5febe8c18c8c8131ed96534cc80cdf4a693dbb9ecce36785a46937b995f4442379a26918380fd739f1090d1044897d690',
  address: 'a5999ac0c7bd6b48ea6ae02aad29371b80a44f70'
}

const request = [{
  txHash: '',
  status: 0,
  version: 'mongox_0.1',
  timestamp: new Date().getTime(), // number
  txFrom: 'a5999ac0c7bd6b48ea6ae02aad29371b80a44f70', // 40 bytes address
  txTo: '8df9628de741b3d42c6f4a29ed4572b0f05fe8b4', // 40 bytes address
  value: '0.01', // string
  gasLimit: '0', // string, temporarily set to 0
  gasUsed: '0', // string, temporarily set to 0
  gasPrice: '0', // string, temporarily set to 0
  txFee: '0',
  nonce: 0,
  extraData: 'Sec test transaction', // string, user defined extra messages
  signature: ''
}]

console.log("timestamp: " + request[0].timestamp)
const tokenTxBuffer = [
  Buffer.from(request[0].version),
  utils.int2Buffer(request[0].timestamp),
  Buffer.from(request[0].txFrom, 'hex'),
  Buffer.from(request[0].txTo, 'hex'),
  Buffer.from(request[0].value),
  Buffer.from(request[0].gasLimit),
  Buffer.from(request[0].gasUsed),
  Buffer.from(request[0].gasPrice),
  Buffer.from(request[0].txFee),
  utils.int2Buffer(request[0].nonce),
  Buffer.from(request[0].extraData)
]

const msgHashBuffer = tokenTxBuffer

let txSigHash = Buffer.from(utils.rlphash(tokenTxBuffer).toString('hex'), 'hex')
let signature = utils.ecsign(txSigHash, Buffer.from(UserInfo.privKey, 'hex'))

request[0].signature = {
  v: signature.v,
  r: signature.r.toString('hex'),
  s: signature.s.toString('hex')
}

tokenTxBuffer.push(Buffer.from(JSON.stringify(request[0].signature)))
request[0].txHash = utils.rlphash(tokenTxBuffer).toString('hex')
console.log("txHash: "+ request[0].txHash)

client.request('getNodeInfo', [], (err, response) => {
  if (err) {
    console.log(err)
  } else {
    console.log(response)
  }
})

client.request('getBlocks', [0, -1], (err, response) => {
  if (err) {
    console.log(err)
  } else {
    console.log(response)
  }
})

client.request('newTx', request, (err, response) => {
  if (err) {
    console.log(err)
  } else {
    console.log(response)
  }
})

client.request('getAccInfo', ['8df9628de741b3d42c6f4a29ed4572b0f05fe8b4'], (err, response) => {
  if (err) {
    console.log(err)
  } else {
    console.log(response)
  }
})

client.request('getBalance', [UserInfo.address], (err, response) => {
  if (err) {
    console.log(err)
  } else {
    console.log(response)
  }
})